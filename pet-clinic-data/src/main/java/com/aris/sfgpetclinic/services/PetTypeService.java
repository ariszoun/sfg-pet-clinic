package com.aris.sfgpetclinic.services;

import com.aris.sfgpetclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType, Long>{
//    PetType findPetTypeByName(String petTypeName);
}
