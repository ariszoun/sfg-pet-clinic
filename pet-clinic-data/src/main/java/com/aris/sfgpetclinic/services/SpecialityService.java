package com.aris.sfgpetclinic.services;

import com.aris.sfgpetclinic.model.Speciality;

public interface SpecialityService extends CrudService<Speciality, Long> {
}
