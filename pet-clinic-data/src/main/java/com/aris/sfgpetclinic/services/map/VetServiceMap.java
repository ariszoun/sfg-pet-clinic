package com.aris.sfgpetclinic.services.map;

import com.aris.sfgpetclinic.model.Speciality;
import com.aris.sfgpetclinic.model.Vet;
import com.aris.sfgpetclinic.services.SpecialityService;
import com.aris.sfgpetclinic.services.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Profile({"default", "map"})
public class VetServiceMap extends AbstractMapService<Vet, Long> implements VetService {

    private final SpecialityService specialityService;

    public VetServiceMap(SpecialityService specialityService) {

        this.specialityService = specialityService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public Vet findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Vet save(Vet vetObject) {
        if(vetObject.getSpecialities().size() > 0) {
            vetObject.getSpecialities().forEach(speciality -> {
                if(speciality.getId() == null) {
                    Speciality savedSpeciality = specialityService.save(speciality); //this will create an id for speciality
                    savedSpeciality.setId(savedSpeciality.getId()); //here we ensure that this id will be provided (defensive coding)
                }
            });
        }
        return super.save(vetObject);
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet object) {
        super.delete(object);
    }

}
