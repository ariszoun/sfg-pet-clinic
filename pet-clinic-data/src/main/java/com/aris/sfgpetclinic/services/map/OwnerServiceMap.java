package com.aris.sfgpetclinic.services.map;

import com.aris.sfgpetclinic.model.Owner;
import com.aris.sfgpetclinic.model.Pet;
import com.aris.sfgpetclinic.services.OwnerService;
import com.aris.sfgpetclinic.services.PetService;
import com.aris.sfgpetclinic.services.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Profile({"default", "map"})
public class OwnerServiceMap extends AbstractMapService<Owner, Long> implements OwnerService {

    private final PetTypeService petTypeService;
    private final PetService petService;

    public OwnerServiceMap(PetTypeService petTypeService, PetService petService) {
        this.petTypeService = petTypeService;
        this.petService = petService;
    }

    @Override
    public Set<Owner> findAll() {

        return super.findAll();
    }

    @Override
    public Owner findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Owner save(Owner ownerObject) {

        Owner savedOwner = null;

        if(ownerObject != null) {
            if(ownerObject.getPets() != null) {
                ownerObject.getPets().forEach(pet -> { //need to iterate through owner's pets
                            if(pet.getPetType() != null) {
                                if(pet.getPetType().getId() == null) { //if there is not that pet type we need to save that pet type
                                    pet.setPetType(petTypeService.save(pet.getPetType()));
                                }
                            } else {
                                throw new RuntimeException("PetType is required");
                            }

                            if(pet.getId() == null) {
                                Pet savedPet = petService.save(pet);
                                pet.setId(savedPet.getId());
                            }
                        });
            }
            return super.save(ownerObject);
        } else {
            return null;
        }


    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Owner object) {
        super.delete(object);
    }

    @Override
    public Owner findByLastName(String lastName) {
        return this.findAll()
                .stream().filter(owner -> owner.getLastName().equalsIgnoreCase(lastName))
                .findFirst().orElse(null);
    }

    @Override
    public List<Owner> findAllByLastNameLike(String lastName) {
        //todo - impl
        return this.findAll().stream()
                .filter(owner -> owner.getLastName().equalsIgnoreCase(lastName))
                .collect(Collectors.toList());
    }
}
