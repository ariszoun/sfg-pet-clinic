package com.aris.sfgpetclinic.services;

import com.aris.sfgpetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long>{

}
