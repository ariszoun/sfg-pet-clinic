package com.aris.sfgpetclinic.services;

import com.aris.sfgpetclinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {


}
