package com.aris.sfgpetclinic.services;

import com.aris.sfgpetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
