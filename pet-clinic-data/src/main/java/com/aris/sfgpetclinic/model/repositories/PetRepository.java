package com.aris.sfgpetclinic.model.repositories;

import com.aris.sfgpetclinic.model.Pet;
import org.springframework.data.repository.CrudRepository;

public interface PetRepository extends CrudRepository<Pet, Long> {
}
