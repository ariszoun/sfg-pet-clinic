package com.aris.sfgpetclinic.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public class BaseEntity implements Serializable {

    public BaseEntity(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //use wrapped types instead of primitive (ie long) cause can have nulls. Primitives cannot have nulls

    //method to check if an entity in newly Added or is not (like template createOrUpdateOwner Line 23)
    public boolean isNew() {
        return this.id == null;
    }
}
