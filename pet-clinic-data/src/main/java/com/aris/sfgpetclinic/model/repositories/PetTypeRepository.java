package com.aris.sfgpetclinic.model.repositories;

import com.aris.sfgpetclinic.model.PetType;
import org.springframework.data.repository.CrudRepository;

public interface PetTypeRepository extends CrudRepository<PetType, Long> {
//    PetType findPetTypeByName(String petTypeName);
}
