package com.aris.sfgpetclinic.model.repositories;

import com.aris.sfgpetclinic.model.Vet;
import org.springframework.data.repository.CrudRepository;

public interface VetRepository extends CrudRepository<Vet, Long> {
}
