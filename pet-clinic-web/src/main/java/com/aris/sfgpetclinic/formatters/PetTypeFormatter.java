package com.aris.sfgpetclinic.formatters;

import com.aris.sfgpetclinic.model.PetType;
import com.aris.sfgpetclinic.services.PetTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Collection;
import java.util.Locale;

@Component
public class PetTypeFormatter implements Formatter<PetType> {

    private final PetTypeService petTypeService;

    public PetTypeFormatter(PetTypeService petTypeService) {
        this.petTypeService = petTypeService;
    }

    @Override
    public String print(PetType petType, Locale locale) {
        return petType.getName();
    }

    @Override
    public PetType parse(String text, Locale locale) throws ParseException {
        //todo-fix find best solution

        //Solution 1////////////////////////////////////////////////////////////
//        return petTypeService.findAll().stream()
//                .filter(petType -> petType.getName().equals(text))
//                .findFirst()
//                .orElseThrow(() -> new ParseException(
//                        "Type not found " + text, 0
//                ));

        //Solution-2////////////////////////////////////////////////////////////
        //New functionality findByPetTypeName to model repository and service
        //return petTypeService.findPetTypeByName(text);



        //Solution-3////////////////////////////////////////////////////////////
        Collection<PetType> petTypes = petTypeService.findAll();

        for(PetType type: petTypes) {
            if(type.getName().equals(text)) {
                return type;
            }
        }
        throw new ParseException("type not found " + text , 0);
    }

}
