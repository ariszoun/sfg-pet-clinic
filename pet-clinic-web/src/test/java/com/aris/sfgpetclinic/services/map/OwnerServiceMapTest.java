package com.aris.sfgpetclinic.services.map;

import com.aris.sfgpetclinic.model.Owner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class OwnerServiceMapTest {

    OwnerServiceMap ownerServiceMap;

    final Long ownerId = 1L;
    final String lastName = "Zounarakis";

    @BeforeEach
    void setUp() {
        ownerServiceMap = new OwnerServiceMap(new PetTypeServiceMap(), new PetServiceMap());
        ownerServiceMap.save(Owner.builder()
                                    .id(ownerId)
                                    .lastName(lastName)
                                    .build());
    }

    @Test
    void findAll() {
        Set<Owner> ownerSet = ownerServiceMap.findAll();
        assertEquals(1, ownerSet.size());
    }

    @Test
    void findById() {
        Owner owner = ownerServiceMap.findById(ownerId);
        assertEquals(ownerId, owner.getId());
    }

    @Test
    void saveExistingId() {
        Long id = 2L;
        Owner owner2 = Owner.builder().id(id).build();
        Owner savedOwner = ownerServiceMap.save(owner2);
        assertEquals(id, savedOwner.getId());
    }

    @Test
    void savedNoId() {
        Owner savedOwner = ownerServiceMap.save(Owner.builder().build());
        assertNotNull(savedOwner);
        assertNotNull(savedOwner.getId());
    }

    @Test
    void deleteById() {
        ownerServiceMap.deleteById(ownerId);
        assertEquals(0, ownerServiceMap.findAll().size());
    }

    @Test
    void delete() {
        //using the ownerServiceMap to turn back the object, so I'm deleting by the object
        ownerServiceMap.delete(ownerServiceMap.findById(ownerId));
        assertEquals(0, ownerServiceMap.findAll().size());
    }

    @Test
    void findByLastName() {

        Owner zounarakis = ownerServiceMap.findByLastName(lastName);
        assertNotNull(zounarakis);
        assertEquals(ownerId, zounarakis.getId());

    }

    @Test
    void findByLastNameLike() {
        Owner owner1 = Owner.builder().id(1L).lastName("zounarakis").build();
        Owner owner2 = Owner.builder().id(2L).lastName("zounarakis").build();
        ownerServiceMap.save(owner1);
        ownerServiceMap.save(owner2);

        List<Owner> ownerList = ownerServiceMap.findAllByLastNameLike("zounarakis");
        assertNotNull(ownerList);
        assertEquals(2, ownerList.size());

    }
}