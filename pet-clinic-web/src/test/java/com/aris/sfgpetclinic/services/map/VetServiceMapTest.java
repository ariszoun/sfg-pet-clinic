package com.aris.sfgpetclinic.services.map;

import com.aris.sfgpetclinic.model.Speciality;
import com.aris.sfgpetclinic.model.Vet;
import com.aris.sfgpetclinic.services.SpecialityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class VetServiceMapTest {

    VetServiceMap vetServiceMap;
    SpecialityServiceMap specialityServiceMap;
    Long vetId = 1L;
    Long specialityId = 1L;


    @BeforeEach
    void setUp() {
        specialityServiceMap = new SpecialityServiceMap();
        specialityServiceMap.save(Speciality.builder().id(specialityId).build());

        vetServiceMap = new VetServiceMap(specialityServiceMap);
        vetServiceMap.save(Vet.builder().id(vetId).specialities(specialityServiceMap.findAll()).build());
    }

    @Test
    void findAll() {
        Set<Vet> vets = vetServiceMap.findAll();
        assertEquals(1, vets.size());
    }

    @Test
    void findById() {
        Vet vet = vetServiceMap.findById(vetId);
        assertEquals(1, vet.getId());
    }

    @Test
    void save() {
        Set<Speciality> specialitySet = new HashSet<>();
        Speciality speciality2 = Speciality.builder().id(2L).description("Pathology").build();
        Speciality speciality3 = Speciality.builder().id(3L).description("Microbiology").build();
        specialitySet.add(speciality2);
        specialitySet.add(speciality3);


        Vet savedVet = Vet.builder().id(2L).firstName("Lazaria").lastName("Chamaidi").specialities(specialitySet).build();
        vetServiceMap.save(savedVet);
        assertEquals(2, vetServiceMap.findAll().size());

        //Prints out vet and specialities
        System.out.println(savedVet.getFirstName() + " " + savedVet.getLastName() + "'s Specialities are: ");
        savedVet.getSpecialities().forEach(speciality -> System.out.println(speciality.getDescription()));
    }

    @Test
    void deleteById() {
        vetServiceMap.deleteById(vetId);
        assertEquals(0, vetServiceMap.findAll().size());
    }

    @Test
    void delete() {
        vetServiceMap.delete(vetServiceMap.findById(vetId));
        assertEquals(0, vetServiceMap.findAll().size());
    }
}