package com.aris.sfgpetclinic.services.map;

import com.aris.sfgpetclinic.model.Owner;
import com.aris.sfgpetclinic.model.Pet;
import com.aris.sfgpetclinic.model.PetType;
import com.aris.sfgpetclinic.model.Visit;
import com.aris.sfgpetclinic.services.PetTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class VisitServiceMapTest {

    VisitServiceMap visitServiceMap;

    PetServiceMap petServiceMap;
    OwnerServiceMap ownerServiceMap;

    Long id = 1L;

    @BeforeEach
    void setUp() {
        petServiceMap = new PetServiceMap();
        Pet pet1 = Pet.builder().id(id).petType(new PetType("dog")).name("Azor").build();
        pet1.setId(id);


        petServiceMap.save(pet1);

        Set<Pet> petSet = petServiceMap.findAll();

        ownerServiceMap = new OwnerServiceMap(new PetTypeServiceMap(), new PetServiceMap());
        Owner owner1 = Owner.builder().id(id).pets(petSet).build();
        pet1.setOwner(owner1);

        ownerServiceMap.save(owner1);

        visitServiceMap = new VisitServiceMap();
        Visit savedVisit = Visit.builder().id(id).description("vaccine").date(LocalDate.now()).pet(petServiceMap.findById(id)).build();
        visitServiceMap.save(savedVisit);
    }

    @Test
    void findAll() {
        Set<Visit> visitSet = visitServiceMap.findAll();
        assertEquals(1, visitSet.size());
    }

    @Test
    void deleteById() {
        visitServiceMap.deleteById(id);
        assertEquals(0, visitServiceMap.findAll().size());
    }

    @Test
    void delete() {
        visitServiceMap.delete(visitServiceMap.findById(id));
        assertEquals(0, visitServiceMap.findAll().size());
    }

    @Test
    void save() {
        Pet pet2 = Pet.builder().id(1L).build();
        Owner owner2 = Owner.builder().id(1L).build();

        pet2.setOwner(owner2);

        Visit visit2 = Visit.builder().pet(pet2).build();

        visitServiceMap.save(visit2);


        assertEquals(2, visitServiceMap.findAll().size());
    }

    @Test
    void findById() {
        assertEquals(1, visitServiceMap.findById(id).getId());
    }
}